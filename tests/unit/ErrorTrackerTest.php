<?php

use TPro\ErrorUtils\ErrorTracker;

/**
 * Class ErrorTrackerTest
 */
class ErrorTrackerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Make sure no exception will be thrown if there is no configuration defined
     */
    public function testTrackExceptionWithoutConfigWontFail()
    {
        $this->assertFalse(defined('SENTRY_DSN'));
        $this->assertFalse(getenv('SENTRY_DSN'));

        $this->tester->assertFalse(ErrorTracker::trackException(new \Exception('Hello World')));
    }

    /**
     * Make sure no exception will be thrown if there is no configuration defined
     */
    public function testTrackMessageWithoutConfigWontFail()
    {
        $this->assertFalse(defined('SENTRY_DSN'));
        $this->assertFalse(getenv('SENTRY_DSN'));

        $this->tester->assertFalse(ErrorTracker::trackMessage('Hello World'));
    }

    /**
     * Make sure no exception will be thrown if there is no configuration defined
     */
    public function testGetRavenClientWithoutConfigWontFail()
    {
        $this->assertFalse(defined('SENTRY_DSN'));
        $this->assertFalse(getenv('SENTRY_DSN'));

        $this->tester->assertNull(ErrorTracker::getRavenClient());
    }

    public function testTrackException()
    {
        $exception = new \Exception('Hello World');
        $contextData = [
            'some' => 'context data',
        ];

        $mock = $this->createRavenClientMock();
        $mock->expects($this->once())
            ->method('captureException')
            ->with($this->equalTo($exception), null, null, $contextData);

        $this->overrideRavenClient($mock);

        ErrorTracker::trackException($exception, $contextData);
    }

    public function testTrackMessage()
    {
        $message = 'Hello World';
        $contextData = [
            'some' => 'context data',
        ];

        $mock = $this->createRavenClientMock();
        $mock->expects($this->once())
            ->method('captureMessage')
            ->with($this->equalTo($message), $contextData);

        $this->overrideRavenClient($mock);

        ErrorTracker::trackMessage($message, $contextData);
    }

    public function testSentryDsnEnvironmentVariableIsRead()
    {
        $this->assertFalse(defined('SENTRY_DSN'));
        $this->assertFalse(getenv('SENTRY_DSN'));

        putenv('SENTRY_DSN=https://12345:abcde@localhost/123');

        // Unset Raven client that might be set by previous tests
        $this->overrideRavenClient(null);

        $client = ErrorTracker::getRavenClient();

        $this->assertInstanceOf(\Raven_Client::class, $client);
        $this->assertEquals('https://localhost/api/123/store/', $client->server);
        $this->assertEquals('123', $client->project);
        $this->assertEquals('12345', $client->public_key);
        $this->assertEquals('abcde', $client->secret_key);
    }

    public function testSentryDsnConstantIsRead()
    {
        // Reset env variable from previous test
        putenv('SENTRY_DSN');

        $this->assertFalse(defined('SENTRY_DSN'));
        $this->assertFalse(getenv('SENTRY_DSN'));

        define('SENTRY_DSN', 'https://7890:qwerty@localhost/567');

        // Unset Raven client that might be set by previous tests
        $this->overrideRavenClient(null);

        $client = ErrorTracker::getRavenClient();

        $this->assertInstanceOf(\Raven_Client::class, $client);
        $this->assertEquals('https://localhost/api/567/store/', $client->server);
        $this->assertEquals('567', $client->project);
        $this->assertEquals('7890', $client->public_key);
        $this->assertEquals('qwerty', $client->secret_key);
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject|\Raven_Client
     */
    private function createRavenClientMock()
    {
        $mock = $this->getMockBuilder(\Raven_Client::class)->getMock();
        return $mock;
    }

    /**
     * @param Raven_Client|null $ravenClient
     */
    private function overrideRavenClient(\Raven_Client $ravenClient = null)
    {
        $reflectedClass = new \ReflectionClass(ErrorTracker::class);
        $reflectedProperty = $reflectedClass->getProperty('ravenClient');
        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue('ravenClient', $ravenClient);
    }
}
