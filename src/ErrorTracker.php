<?php

namespace TPro\ErrorUtils;

/**
 * Self-configurable, Sentry-based error tracker.
 * Because we truly hope there will be only one error per billion calls,
 * this class intentionally made independent from the basic environment, so there won't be need to put it's initialization into bootstrap file.
 *
 * Class ErrorTracker
 * @package TPro\ErrorUtils
 */
class ErrorTracker
{
    /**
     * @var \Raven_Client|null
     */
    private static $ravenClient;

    /**
     * Sends given message into Sentry Error Tracker
     *
     * @NB! This method works silently! It triggers no exceptions if something goes wrong.
     * The only chance to get know was the error tracked or not is to check a result this method returns.
     *
     * @param string $message
     * @param null|array $context
     *
     * @return bool
     */
    public static function trackMessage($message, array $context = [])
    {
        if (null === $ravenClient = self::getRavenClient()) {
            return false;
        }

        try {
            $ravenClient->captureMessage($message, $context);
        } catch (\Exception $e) {
            // Exit silently,
            // nobody expects that Error Tracker will throw an exception instead of tracking errors
            return false;
        }

        return true;
    }

    /**
     * Sends given exception into Sentry Error Tracker
     *
     * @NB! This method works silently! It triggers no exceptions if something goes wrong.
     * The only chance to get know was the error tracked or not is to check a result this method returns.
     *
     * @param \Exception $e
     * @param null|array $context
     *
     * @return bool
     */
    public static function trackException(\Exception $e, array $context = [])
    {
        if (null === $ravenClient = self::getRavenClient()) {
            return false;
        }

        try {
            $ravenClient->captureException($e, null, null, $context);
        } catch (\Exception $e) {
            // Exit silently,
            // nobody expects that Error Tracker will throw an exception instead of tracking errors
            return false;
        }

        return true;
    }

    /**
     * Gets Raven Client instance.
     * Raven Client is a client lib to communicate with Sentry Error tracker API.
     * Returns null, if client can't be instantiated, e.g. config is missing.
     *
     * @return null|\Raven_Client
     */
    public static function getRavenClient()
    {
        // Check if client is already initialized
        if (self::$ravenClient !== null) {
            // Return already initialized instance
            return self::$ravenClient;
        }

        // Lookup for configuration
        if (getenv('SENTRY_DSN')) {
            $sentryDsn = getenv('SENTRY_DSN');
        } elseif (defined('SENTRY_DSN')) {
            $sentryDsn = SENTRY_DSN;
        }

        // We have no DSN string, nothing to do here, return
        if (empty($sentryDsn)) {
            return null;
        }

        // Create new client instance
        self::$ravenClient = new \Raven_Client($sentryDsn, [
            'processorOptions' => [
                // @TODO: make this list configurable
                'Raven_SanitizeDataProcessor' => [
                    // This cuts out the values with listed key names
                    'fields_re' => '/(token|api_key)/i',
                    // This cuts out everything that looks like PAN
                    'values_re' => '/^(?:\d[ -]*?){14,16}$/'
                ]
            ]
        ]);

        // Application version
        if (defined('APP_VERSION')) {
            self::$ravenClient->setRelease(APP_VERSION);
        }

        // Environment application runs in
        if (defined('ENVIRONMENT')) {
            self::$ravenClient->setEnvironment(ENVIRONMENT);

        }

        // Our production Sentry installation works with self-signed cert
        // therefore we will force SSL check to be disabled
        self::$ravenClient->verify_ssl = false;

        return self::$ravenClient;
    }
}
