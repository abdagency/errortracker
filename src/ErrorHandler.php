<?php

namespace TPro\ErrorUtils;

/**
 * Class ErrorHandler
 * @package TPro\ErrorUtils
 */
class ErrorHandler
{
    /**
     * Registers error and exception handlers
     */
    public static function install()
    {
        // Register global exception handler
        set_exception_handler([__CLASS__, 'handleException']);

        // Setup error handler
        set_error_handler(function ($code, $message, $file, $line) {
            // Simply re-throwing an exception, to give all controls to exception handler
            throw new \ErrorException($message, 0, $code, $file, $line);
        });

        // Check for PHP fatal errors on PHP shutdown
        register_shutdown_function(function () {

            if (null === ($error = error_get_last()) || E_ERROR !== $error['type']) {
                return;
            }

            if (null !== $ravenClient = ErrorTracker::getRavenClient()) {
                $ravenClient->captureLastError();
            }
        });
    }

    /**
     * Handles exception
     *
     * @param \Exception $e
     */
    public static function handleException(\Exception $e)
    {
        // Track error with Sentry error tracker
        ErrorTracker::trackException($e);

        // Set the appropriate response code
        http_response_code(500);
        // Stop with general error message
        die('Internal Server Error');
    }
}
